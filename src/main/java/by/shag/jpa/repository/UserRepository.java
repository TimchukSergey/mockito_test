package by.shag.jpa.repository;

import by.shag.jpa.model.User;

import java.util.List;

public class UserRepository {

    public User save(User user) {
        throw  new UnsupportedOperationException();
    }

    public User findById(Integer id) {
        throw  new UnsupportedOperationException();
    }

    public List<User> findAll() {
        throw  new UnsupportedOperationException();
    }

    public User update(User user) {
        throw  new UnsupportedOperationException();
    }

    public void delete(Integer id) {
        throw  new UnsupportedOperationException();
    }
}
